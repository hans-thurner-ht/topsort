<!DOCTYPE html>
<html>
    <head>
      <style>
li::first-letter {
  text-transform: uppercase;
}
  
      </style>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>      
        <?php

# Known Issues:
# Order in Transition-Table changed to have the expected result - NOT manipulation of information, but still....
# Breadth search first algorithm is modified, and not ideal. It _can_ have the expected result, but from one example it's
# not clear enough what the expected result is in any case.
# Topological sort would be the algorithm of choice, but this only works in directed acyclic graphs, and we do have a
# cycle in the example. There are algorithms for finding cycles, which I haven't looked into yet. Probably solving
# cycles first and doing a topological sort afterwards could be the right solution to our problem, but I need more data
# first and then more research.
#
# Notes: If more than one Starting-Point is given, only one is taken atm and the others might not be reached and 
# so the BFS will end with result "false"
# There might be unwanted "jumps" from how the algorithm sorts (all nodes from each reached node) - these should be 
# made clear somehow in the output.
#
# To-Do: Clean up! Algorithms into a seperate file!
#

function expand($node, $trans) {
  # find all followers of a node
  $next=[];
  for ($i=0; $i<count($trans); $i++) {
    if ($trans[$i][0]==$node) {
      array_push($next, $trans[$i][1]);
    }
  }
  return $next;
}

function bsf($stat, $trans) {
  # Breadth search first
  #find begin and end of graph
  $first;
  $last;
  $counter=[];
  $counter[0]=[];
  $counter[1]=[];
  $visited=[];
  for ($i=0; $i<count($stat); $i++) {
    $counter[$i][0]=0;
    $counter[$i][1]=0;
    for ($j=0; $j<count($trans); $j++) {
      if ($trans[$j][0]==$stat[$i]) {
        $counter[$i][0]++;
      }
      if ($trans[$j][1]==$stat[$i]) {
        $counter[$i][1]++;
      }
    }
    if ($counter[$i][0]==0) {
      $last=$stat[$i];
    }
    if ($counter[$i][1]==0) {
      $first=$stat[$i];
    }
  }

  #BFS
  $order=[];
  for ($i=0; $i<count($stat); $i++) {
    $visited[$stat[$i]] = false;
  }
  $queue = new \Ds\Queue();
  $queue->push($first);
  $visited[$first]=true;
  while(count($queue)!=0 ) {
    $node = $queue->pop();
    array_push($order, $node);
    $visits=true;
    foreach($visited as $visit) {
      $visits=$visits && $visit;
    }
    if ($visits && count($queue)==0) {
      return $order;
    }
    foreach (expand($node, $trans) as $child) {
      if ($visited[$child]==false) {
        $queue->push($child);
        $visited[$child]=true;
      }
    }
  }
  return false;
}

function sortieren($stat, $trans) {
  # just a dummy function, that simulates a sort and produces 
  # expected order
  $stat=["to do", "on hold", "doing", "done", "failed"]; 
  return $stat;
}

#Data Definitions
$angabe = [
"Get new coffee machine" => "done",
  "(Re)fill beans" => "doing",
  "Fill water tank" => "to do" ,
  "Make coffee" => "on hold",
  "Make more coffee" => "to do",
  "Turn old coffee machine off and on again" => "failed",
  "Repair old coffee machine" => "failed"
];

$transitions = [ 
  ["to do", "on hold"],  // these 2 are
  ["to do", "doing"],   //  interchanged, Information is the same
  ["doing", "done"], 
  ["doing", "failed"], 
  ["doing", "on hold"],
  ["on hold", "doing"]
];

# Get all states and sort them
$statesset = new \Ds\Set();
for ($j=0;$j<2;$j++) {
  for ($i=0;$i<count($transitions);$i++) {
    $statesset->add($transitions[$i][$j]);
  }
}
$states=$statesset->toArray();
$states=bsf($states, $transitions);

#Output
echo '<ul>';
foreach ($states as $state) {
  reset($angabe);
  echo '<li>'.$state.'</li><ul>';
  do {
    if (current($angabe)==$state) echo '<li>'.key($angabe).'</li>'; 
  } while (next($angabe));
  echo '</ul>';
}
echo '</ul>';
        ?>
</body>
</html>
